* OS: Fedora21 3.19.7-200.fc21.x86_64
* CPU: Intel Corei7 920
* GPU: GeForce GTX 680
* RAM: 12GB (TS1333KLU-6GK x3 + TS1333KLU-6GK x3)
* Mother: MSI X58M (MS-7593)

* RapidCFD-dev rev: 874069affafdd26d93e5bf8548137185a14841ed
* ThirdParty-2.3.1
* VandV rev: e5b8f9d3e92d3f4e730eb873e062f53707b62c58 + fieldAverage1なし
* CUDA SDK: 7.0
* gcc 4.9.2
* OpenMPI 1.8.4
