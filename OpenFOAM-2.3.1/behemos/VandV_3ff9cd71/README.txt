* OS: Ubuntu 14.04.1 LTS
* CPU: Intel Corei7 2600K
* RAM: 16GB

* OpenFOAM-2.3.1
* ThirdParty-2.3.1
* VandV rev: 3ff9cd71aaef6a746b15f1de7f8f46e259a03e91 + fieldAverage1なし
* gcc 4.8.2
* OpenMPI 1.6.5
