* OS: Fedora21 3.19.7-200.fc21.x86_64
* CPU: Intel Corei7 920
* RAM: 12GB (TS1333KLU-6GK x3 + TS1333KLU-6GK x3)
* Mother: MSI X58M (MS-7593)

* OpenFOAM-2.3.1
* ThirdParty-2.3.1
* VandV rev: 3ff9cd71aaef6a746b15f1de7f8f46e259a03e91 + fieldAverage1なし
* gcc 4.9.2
* OpenMPI 1.8.3
